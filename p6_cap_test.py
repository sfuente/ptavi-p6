import pyshark
import json
import sys

class Test_Capture():
# We read the control capture
    def __init__(self, cap='invite.libpcap'):
        with open('capture_test.json', 'r') as infile:
            self.test_capture = json.loads(infile.read())
        self.p6_capture = pyshark.FileCapture(cap, use_json=True)
        self.sipFields = []
        self.test_sipFields = []
        self.pkt_len_Output = 'Nº de paquetes: {0}.............{1}'
        self.pkt_Output = 'Paquete {0}.............{1}'


    def parse_capture(self, capture):
        fields_array = []
        for packet in capture:
            packet_list = [x for x in packet.layers if x._layer_name == "sip"]
            if packet_list != []:
                try:
                    fields_array.append(packet_list[0]._all_fields["sip.Request-Line"])
                except KeyError:
                    try:
                        fields_array.append(packet_list[0]._all_fields["sip.Status-Line"])
                    except Exception as e:
                        raise e
        # print(fields_array)
        return fields_array
    
    def check_len(self, pkt, test_pkt):
        is_correct = len(pkt) == len(test_pkt)
        result = "OK" if is_correct else "FAILED"
        print(self.pkt_len_Output.format(len(pkt), result))

    def check_general_packet(self, pkt, test_pkt, num):
        is_correct = pkt == test_pkt
        result = "OK" if is_correct else "FAILED"
        print(self.pkt_Output.format(num, result))

    def test_p6(self):
        self.sipFields = self.parse_capture(self.p6_capture)
        self.test_sipFields = self.test_capture
        self.check_len(self.sipFields, self.test_sipFields)
        for i in range(len(self.sipFields)):
            self.check_general_packet(
                self.sipFields[i], self.test_sipFields[i], i+1)
            
        self.p6_capture.close()
    
if __name__ == "__main__":
    if len(sys.argv) == 2:
        p6_test = Test_Capture(sys.argv[1])
        p6_test.test_p6()
    else:
        p6_test = Test_Capture()
        p6_test.test_p6()

