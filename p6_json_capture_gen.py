import pyshark
import json
import sys

packets = []
sipFields = []


class MyEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__

def generate_capture(capture_file):
    capture = pyshark.FileCapture(capture_file, use_json=True)    
    messages = parse_capture(capture)     
    capture.close()
    with open('capture_test.json', 'w') as outfile:
        json.dump(messages, outfile, indent=3)


def generate_Json_capture(capture_file):
    capture = pyshark.FileCapture(capture_file, use_json=True)

    for packet in capture:
        packets.append(packet)

    with open('capture_test.json', 'w') as outfile:
        json.dump(packets, outfile, indent=3, cls=MyEncoder)

    capture.close()


def parse_capture(capture):
    fields_array = []
    for packet in capture:
        packet_list = [x for x in packet.layers if x._layer_name == "sip"]
        if packet_list != []:
            try:
                fields_array.append(
                    packet_list[0]._all_fields["sip.Request-Line"])
            except KeyError:
                try:
                    fields_array.append(
                        packet_list[0]._all_fields["sip.Status-Line"])
                except Exception as e:
                    raise e
        
    print(fields_array)
    return fields_array

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 p5_json_capture_gen.py capture.pcapng")
    capture = sys.argv[1]
    try:
        generate_capture(capture)
    except Exception as e:
        raise e
