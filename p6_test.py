import os
import signal
import threading
import sys
import time
import subprocess
import pyshark
from p6_cap_test import Test_Capture

DEVICE = os.name
WIN_IFACE = '/Device/NPF_Loopback'
LINUX_IFACE = 'loopback'

test_e2e = lambda : subprocess.run('python p6_e2e_test.py', shell=True, check=True)

def test_p6():
    try:
        threads = []
        sniffer_thread = threading.Thread(target=init_cap)
        sniffer_thread.daemon = True
        sniffer_thread.start()        
        threads.append(sniffer_thread)
    except:
        raise SystemExit
    
    try:
        e2e_thread = threading.Thread(target=test_e2e)
        e2e_thread.daemon = True
        time.sleep(1)
        e2e_thread.start()
        threads.append(e2e_thread)
        time.sleep(2)
        e2e_thread.join()
    except SystemExit:
       raise Exception


def raise_sigint():
    if hasattr(signal, 'CTRL_C_EVENT'):
        #  WINDOWS
        os.kill(os.getpid(), signal.CTRL_C_EVENT)
    else:
        # UNIX
        pgid = os.getpgid(os.getpid())
        if pgid == 1:
            os.kill(os.getpid(), signal.SIGINT)
        else:
            os.killpg(os.getpgid(os.getpid()), signal.SIGINT)

def init_cap():
    print('SNIFFING...')
    iface = WIN_IFACE if DEVICE == 'nt' else LINUX_IFACE
    cap = pyshark.LiveCapture(
        interface=iface, output_file='test.pcapng')
    cap.sniff(timeout=15)
    test_cap = Test_Capture('test.pcapng')
    test_cap.test_p6()
    print('END SNIFFING...')
    raise_sigint()

if __name__ == "__main__":
    try:
        test_p6()
                        
    except KeyboardInterrupt:
        print('KeyboardInterrupt OK')
        

